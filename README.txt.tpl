
# v$ver, $date

Download: https://Seppo.Social/Linux-x86_64-$ver
Source:   https://Seppo.Social/v/$git_sha
Install:  https://seppo.social/en/#installation

Changes

- create a post from the commandline
- visible at a follower on mastodon (digitalcourage.social/@mro)


Corresponds to https://blog.mro.name/2022/12/nlnet-seppo/#2-new-post-via-cli

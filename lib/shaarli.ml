(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * shaarli.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( >>= ) = Result.bind
let chain a b =
  let f a = Ok (a, b) in
  Result.bind a f
let to_channel oc l = l
                      |> Markup.of_list
                      |> Markup.pretty_print
                      |> Markup.write_xml
                      |> Markup.to_channel oc;
  Ok ()

module Login = struct
  let path = "/shaarli/login"

  let form tit tok retu =
    let retu = match retu with
      | None -> ""
      | Some v -> v
    and elm name atts = `Start_element (("", name), atts)
    and att n v = (("", n), v) in
    let name s  = att "name" s
    and type_ s = att "type" s
    and input atts = elm "input" atts in
    [
      `Xml { Markup.version = "1.0";
             encoding = Some "utf-8";
             standalone = Some false;
           };
      `PI ( "xml-stylesheet", "type='text/xsl' href='../../themes/current/loginform.xslt'" );
      `Text [ "\n" ];
      `Comment "
must be compatible with
https://code.mro.name/github/Shaarli-Vanilla/src/master/tpl/loginform.html

https://code.mro.name/mro/ShaarliOS/src/1d124e012933d1209d64071a90237dc5ec6372fc/ios/ShaarliOS/API/ShaarliCmd.m#L386
";
      `Text [ "\n" ];
      elm "html" [ att "xml:base" "../../"; att "xmlns" "http://www.w3.org/1999/xhtml" ];
      elm "head" [];
      elm "title" []; `Text [ tit ]; `End_element;
      `End_element;
      elm "body" [];
      elm "form" [ name "loginform"; att "method" "post" ];
      input [ name "login"; type_ "text" ]; `End_element;
      input [ name "password"; type_ "password" ]; `End_element;
      input [ name "longlastingsession"; type_ "checkbox" ]; `End_element;
      input [ name "token"; type_ "hidden"; att "value" tok ]; `End_element;
      input [ name "returnurl"; type_ "hidden"; att "value" retu ]; `End_element;
      input [ att "value" "Login"; type_ "submit" ]; `End_element;
      `End_element;
      `End_element;
      `End_element;
    ]

  let get oc (tok, (r : Http.Request.t)) =
    let tit = "Title" in
    (Ok (200, "Ok", [ Http.H.ct_xml ])) |> Http.head oc;
    let ur = r |> Http.Request.request_uri |> Uri.of_string in
    form tit tok ("returnurl" |> Uri.get_query_param ur)
    |> Markup.of_list
    |> Markup.pretty_print
    |> Markup.write_xml
    |> Markup.to_channel oc;
    Ok ()

  let cookie_name = "#Seppo!"

  let cookie_timeout tnow =
    30 * 60
    |> Ptime.Span.of_int_s
    |> Ptime.add_span tnow
    |> Option.get

  let cookie_to_string (uid, t) =
    Logr.debug (fun m -> m "create cookie");
    let t = t |> Ptime.to_rfc3339 in
    let open Csexp in
    List [ Atom uid; Atom t ]
    |> to_string

  let cookie_from_string c =
    let open Csexp in
    match c |> parse_string with
    | Ok List [ Atom uid; Atom t ] -> (
        match t |> Ptime.of_rfc3339 with
        | Error _ -> Error "not valid rfc3339"
        | Ok (t,  _, _) -> Ok (uid, t))
    | _ -> Error "ouch"

  let cookie_make (req : Http.Request.t) =
    Cookie.to_string
      ~domain:req.host
      ~http_only:true
      ~path:req.script_name
      ~same_site:`Strict
      ~secure:true
      cookie_name

  let post tnow ban _oc (_tok, (frm, (req : Http.Request.t))) =
    let sleep = 2 in
    Logr.debug (fun m -> m "Shaarli.Login.post, sleep %d seconds..." sleep);
    Unix.sleep sleep;
    let foldl r = function
      | (("login", [ _ ]) as v)
      | (("password", [ _ ]) as v)
      | (("returnurl", [ _ ]) as v)
      | (("token", [ _ ]) as v) -> r |> List.cons v
      | (f, _) -> Logr.debug (fun m -> m "unconsumed form field: '%s'" f); r
    and cmp (a, _) (b, _) = String.compare a b in
    match frm |> List.fold_left foldl [] |> List.sort cmp with
    | [
      ("login", [ uid ]);
      ("password", [ pwd ]);
      ("returnurl", [ retu ]);
      ("token", [ _ ]);
    ] ->
      Ok (uid, pwd)
      >>= Auth.chk_file Auth.fn
      >>= (fun uid ->
          Cfg.CookieSecret.(make "" >>= from_file)
          >>= chain (Ok uid))
      >>= (fun (uid, sec) ->
          let tend = tnow |> cookie_timeout
          and nonce = Cookie.random_nonce () in
          (uid, tend)
          |> cookie_to_string
          |> Cstruct.of_string
          |> Cookie.encrypt sec nonce
          |> Result.ok)
      |> (function
          | Ok cv ->
            Http.s302' [ ("Set-Cookie", cookie_make req cv) ] retu
          | Error "invalid username or password" ->
            ban tnow req.remote_addr;
            Logr.info (fun m -> m "TODO send loginform 0)");
            Http.s403
          | Error e ->
            Logr.err (fun m -> m "%s" e);
            Http.s500)
    | _ ->
      Logr.info (fun m -> m "TODO send loginform 1)");
      Http.s401
end


module Logout = struct
  let path = "/shaarli/logout"

  let get _oc ((_ : string option), req) =
    Http.s302' [ ("Set-Cookie", Login.cookie_make req "") ] "../.."
end


let check_token exp v =
  Logr.debug (fun m -> m "Shaarli.check_token");
  match v with
  | ( ("token", [ _ as tok ] ) :: _, _) as v ->
    if String.equal exp tok
    then Ok (tok, v)
    else Http.s403
  | (_ :: ("token", [ _ as tok ] ) :: _, _) as v ->
    if String.equal exp tok
    then Ok (tok, v)
    else Http.s403
  | _ ->
    Logr.info (fun m -> m "check_token: token form not matched");
    Http.s400

(** get uid from session if still running *)
let ases tnow (r : Http.Request.t) =
  Logr.debug (fun m -> m "Shaarli.ases");
  let uid : string option = match r.http_cookie |> Cookie.of_string with
    (* check if the session cookie carries a date in the future *)
    | ("#Seppo!" as n, pay) :: [] ->
      assert (n = Login.cookie_name);
      let sec = Cfg.CookieSecret.(make "" >>= from_file)
                |> Result.get_ok in
      Option.bind
        (Cookie.decrypt sec pay)
        (fun c ->
           Logr.debug (fun m -> m "cookie value '%s'" c);
           match c |> Login.cookie_from_string with
             Ok (uid, tend) ->
             if tend > tnow
             then
               (Logr.debug (fun m -> m "Shaarli.ases session valid");
                Some uid)
             else None
           | _ -> None)
    | _ ->
      Logr.debug (fun m -> m "#Seppo! cookie not found.");
      None
  in Ok (uid, r)

module Config = struct
  let path = "/shaarli/config"

  type t = {
    title          : string;
    setlogin       : string;
    setpassword    : string;
    confirmpassword: string;
  }

  let form r =
    let r = match r with
      | Ok r -> r
      | Error (_, r) -> r
    and elm name atts = `Start_element (("", name), atts)
    and att n v = (("", n), v) in
    let name s  = att "name" s
    and type_ s = att "type" s
    and input atts = elm "input" atts in
    [
      `Xml {
        Markup.version = "1.0";
        encoding = Some "utf-8";
        standalone = Some false;
      };
      `PI ("xml-stylesheet", "type='text/xsl' href='../../themes/current/configform.xslt'");
      `Text [ "\n" ];
      `Comment "
The html you see here is for compatibility with vanilla shaarli.

The main reason is backward compatibility for e.g. http://mro.name/ShaarliOS and
https://github.com/dimtion/Shaarlier
";
      `Text [ "\n" ];
      elm "html" [ att "xmlns" "http://www.w3.org/1999/xhtml" ];
      elm "head" []; `End_element;
      elm "body" [];
      elm "form" [ att "method" "post"; name "configform"; att "id" "configform" ];
      input [ name "setlogin";        type_ "text"; att "value" r.setlogin ]; `End_element;
      input [ name "setpassword";     type_ "password" ]; `End_element;
      input [ name "confirmpassword"; type_ "password" ]; `End_element;
      input [ name "title";           type_ "text"; att "value" r.title ]; `End_element;
      input [ name "Save";            type_ "submit"; att "value" "Save config" ]; `End_element;
      `End_element;
      `End_element;
      `End_element;
    ]

  let get oc (_tok, (uid, _req)) =
    let _need_uid = (Auth.is_setup Auth.fn) in
    (* TODO load auth & profile *)
    let r' = {
      title           = "My #Seppo!";
      setlogin        = uid;
      setpassword     = "\n";
      confirmpassword = "";
    } in
    Ok (200, "Ok", [ Http.H.ct_xml ])
    |> Http.head oc;
    Ok r'
    |> form
    |> to_channel oc

  let to_t_validated r (frm : Http.Form.t) : (t, 'a) result =
    let of_frm r = function
      | ("title",           v :: _) -> {r with title = v}
      | ("setlogin",        v :: _) -> {r with setlogin = v}
      | ("setpassword",     v :: _) -> {r with setpassword = v}
      | ("confirmpassword", v :: _) -> {r with confirmpassword = v}
      | (f, _) -> Logr.debug (fun m -> m "unconsumed form field: '%s'" f); r
    and s rx ma x =
      let mi = 1 in
      assert (mi <= ma);
      let l = x |> String.length in
      mi <= l && l <= ma && Re.execp rx x
    and chk pred e r = if pred
      then Ok r
      else Error (e, r)
    and r_id =
      (* TODO limit to one of
       * http://unicode.org/reports/tr31/?
       * https://ocaml.org/manual/lex.html#ident
       * https://www.w3.org/TR/xml/#NT-Name
       * https://www.w3.org/TR/activitypub/#obj-id
       * 'unreserved' https://www.rfc-editor.org/rfc/rfc3986#appendix-A
      *)
      Re.Pcre.regexp {|^[a-zA-Z0-9_.-]+$|}
    and r_1 =
      (* single line *)
      Re.Pcre.regexp {|^\S([^\n\t]*\S)?$|}
    in 
    frm
    |> List.fold_left of_frm r
    |> Result.ok
    >>= fun r -> chk (s r_1  100 r.title)       "title" r
    >>= fun r -> chk (s r_id  20 r.setlogin)    "setlogin" r
    >>= fun r -> chk (s r_1  150 r.setpassword) "setpassword" r
    >>= fun r -> chk (String.equal r.setpassword r.confirmpassword) "confirmpassword" r

  let post _ oc (_tok, (frm, (_uid, (req : Http.Request.t)))) =
    let _boo = (Auth.is_setup Auth.fn) in
    Logr.debug (fun m -> m "Shaarli.Config.post");
    assert (Http.Mime.app_form_url = req.content_type);
    match frm |> to_t_validated {
        title           = "\n";
        setlogin        = "\n";
        setpassword     = "\n";
        confirmpassword = "";
      } with
    | Error _ as ee ->
      Http.s422x |> Http.head oc;
      ee |> form |> to_channel oc
    | Ok r ->
      let _ = Cfg.Profile.(from_file target)
      (*  >>= fun p -> Ok {p with name = r.title} *)
        >>= Cfg.Profile.(to_file target) in
      let _ = Ok (r.setlogin, r.setpassword)
        >>= Auth.(to_file fn) in
      let _ = Ok (req |> Http.Request.base)
        >>= Cfg.Base.(to_file fn) in
      let _ = As2.Person.make "" in
      Http.s302 (req.script_name ^ "/..")
end

(** if no uid redirect to login page *)
let uid_redir = function
  | (Some uid, r) -> Ok (uid, r)
  | (None, (r : Http.Request.t)) ->
    if Auth.is_setup Auth.fn
    then
      let retu = r |> Http.Request.abs in
      Http.s302 (r.script_name ^ Login.path ^ "?returnurl=" ^ retu (* TODO url encode! *))
    else (
      if Config.path = r.path_info
      then (
        Logr.info (fun m -> m "auth is not set up yet, so go on with an empty uid. %s" r.path_info);
        Ok ("", r))
      else
        let retu = r |> Http.Request.abs in
        Http.s302 (r.script_name ^ Config.path ^ "?returnurl=" ^ retu (* TODO url encode! *))
    )

module Post = struct
  let path = "/shaarli"

  let form tit tok =
    let elm name atts = `Start_element (("", name), atts)
    and att n v = (("", n), v) in
    let name s  = att "name" s
    and type_ s = att "type" s
    and input atts = elm "input" atts in
    [
      `Xml { Markup.version = "1.0";
             encoding = Some "utf-8";
             standalone = Some false;
           };
      `PI ( "xml-stylesheet", "type='text/xsl' href='../../themes/current/linkform.xslt'" );
      `Text [ "\n" ];
      `Comment "
";
      `Text [ "\n" ];
      elm "html" [ att "xml:base" "../../"; att "xmlns" "http://www.w3.org/1999/xhtml" ];
      elm "head" [];
      elm "title" []; `Text [ tit ]; `End_element;
      `End_element;
      elm "body" [];
      elm "form" [ name "linkform" ];
      input [ name "post"; type_ "text" ]; `End_element;
      input [ name "token"; type_ "hidden"; att "value" tok ]; `End_element;
      input [ att "value" "Add"; type_ "submit" ]; `End_element;
      `End_element;
      `End_element;
      `End_element;
    ]

  (* only parameter is 'post' 
   * https://code.mro.name/github/Shaarli-Vanilla/src/master/index.php#L427
   * https://code.mro.name/github/Shaarli-Vanilla/src/029f75f180f79cd581786baf1b37e810da1adfc3/index.php#L1548
  *)
  let get oc (tok, (_uid, _req)) =
    let tit = "Add" in
    (* - look up url in storage
     * - if not present:
     *   - if title not present
     *     then
     *       try to get from url
     *       use title, description, keywords
     * - show 'linkform'
    *)
    (Ok (200, "Ok", [ Http.H.ct_xml ])) |> Http.head oc;
    form tit tok
    |> Markup.of_list
    |> Markup.pretty_print
    |> Markup.write_xml
    |> Markup.to_channel oc;
    Ok ()

  (* https://code.mro.name/github/Shaarli-Vanilla/src/master/index.php#L1479 *)
  let post _ _oc (_tok, (_frm, (_uid, (_req : Http.Request.t)))) =
    Http.s501
end

module Tools = struct
  let get _ = Http.s501
end

module Session = struct
  let get oc (uid, _req) =
    match uid with
    | None -> (* no ban penalty! *) Http.s404
    | Some v -> Ok (output_string oc v)
end


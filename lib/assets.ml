(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

(** Assets packed into the binary and to be unpacked and used as they are *)
module Const = struct
  (** Unpack the web application assets from the executable. *)

  let delete_unpack_marker = "delete-me-to-unpack-missing"

  let mutabl =
    [
      "app/var/lib/me-avatar.jpg";
      "app/var/lib/me-banner.jpg";
      Cfg.Profile.target;
      Cfg.Urlcleaner.fn;
    ]

  let all =
    mutabl @ [
      ".htaccess";
      "README.txt";
      "activitypub/README.txt";
      "app/.htaccess";
      "app/README.txt";
      "app/etc/ban.cfg";
      "app/i-must-be-403.svg";
      "app/var/README.txt";
      "app/var/lib/o/d/README.txt";
      "app/var/lib/o/m/README.txt";
      "app/var/lib/o/p/README.txt";
      "app/var/lib/o/t/README.txt";
      Storage.target;
      Storage.fn_idx |> (fun (Mapcdb.Cdb v) -> v);
      Storage.fn_url |> (fun (Mapcdb.Cdb v) -> v);
      Ban.fn;
      "app/var/log/seppo.log";
      "app/var/log/seppo.log.0";
      "app/var/run/README.txt";
      "app/var/spool/README.txt";
      "contrib/README.txt";
      "contrib/etc/lighttpd/conf-available/50-seppo.conf";
      "contrib/harden.sh";
      delete_unpack_marker;
      "favicon.ico";
      "index.html";
      "robots.txt";
      "seppo.pub.pem";
      "themes/current/404.html";
      "themes/current/README.txt";
      "themes/current/configform.xslt";
      "themes/current/do=tools.xslt";
      "themes/current/linkform.xslt";
      "themes/current/loginform.xslt";
      "themes/current/posts.css";
      "themes/current/posts.js";
      "themes/current/posts.xslt";
      "themes/current/style.css";
    ]

  (* how to deal with IO erros? Let them fly! *)
  let restore_if_nonex perm candidates =
    Logr.debug (fun m -> m "Assets.restore_if_nonex");
    let restore_file fn =
      let _ = fn |> Filename.dirname |> File.mkdir_p File.pDir in
      if not (File.exists fn) then
        File.out_channel false ~perm fn (fun oc ->
            match Res.read ("static/" ^ fn) with
            | None ->
              Logr.err (fun m -> m "missing %s" fn);
            | Some str ->
              str |> output_string oc;
              Logr.info (fun m -> m "unpacked %s" fn)
          )
    in
    (if File.exists delete_unpack_marker
     then []
     else candidates)
    |> List.iter restore_file
end

(** Generated assets with local dependencies *)
module Gen = struct
  let make x =
    Result.bind 
      As2.Webfinger.(Make.make "" rulez target)
      (fun _ -> Ok x)

  (* ready to be used in the handler. *)
  let make_cgi x =
    match make x with
    | Error msg ->
      Logr.err (fun m -> m "%s" msg);
      Http.s500
    | Ok _ as o -> o
end


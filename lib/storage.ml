(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( let* ) = Result.bind
let pre = "app/var/lib/"
let target = pre ^ "o/p.s"
let fn_idx = Mapcdb.Cdb (pre ^ "o/id.cdb")
let fn_url = Mapcdb.Cdb (pre ^ "o/url.cdb")
let fn_t   = Mapcdb.Cdb (pre ^ "o/t.cdb")

module Fifo = struct
  type t        = string * int

  let make size fn =
    (fn,size)

  let push byt (fn,size) =
    let sep = '\n' in
    let len = byt |> Bytes.length in
    let keep = size - len - 1 in
    let mode = [ Open_append; Open_binary; Open_creat; Open_wronly ] in
    if keep < try (Unix.stat fn).st_size with _ -> 0
    then (* make space and add *)
      let ret = len |> Bytes.create in
      let buf = keep |> Bytes.create in
      File.in_channel fn (fun ic ->
          really_input ic ret 0 len;
          let _ = input_char ic in
          really_input ic buf 0 keep );
      File.out_channel true ~mode fn (fun oc ->
          output_bytes oc buf;
          output_bytes oc byt;
          output_char oc sep
        );
      Some ret
    else (* just add *)
      (File.out_channel false ~mode fn (fun oc ->
           output_bytes oc byt;
           output_char oc sep
         );
       None)
end

(* a tuple of two (file) positions *)
module TwoPad10 = struct
  let encode (a,b) =
    Printf.sprintf "0x%08x-0x%08x" a b

  let two a b = (a,b)

  let decode s =
    Scanf.sscanf s "%i-%i" two

  let from_channel ic =
    let r = ref ([]) in
    (try while true do
         let tw = ic |> input_line |> decode in
         r := tw :: !r
       done
     with End_of_file -> ());
    !r
end

module Feed = struct
  let pat = Str.regexp {|^\(.+\)-\([0-9]+\)\.idx$|}

  let max_page_index (dir,fi) =
    let d = dir |> Unix.opendir in
    let mx = ref (-1) in
    (try
       while true do
         let dir = Unix.readdir d in
         if Str.string_match pat dir 0
         then
           let g1 = dir |> Str.matched_group 1 in
           if g1 |> String.equal fi
           then
             let g2 = dir |> Str.matched_group 2 in
             let n = g2 |> int_of_string in
             mx := max !mx n
       done
     with End_of_file -> ());
    d |> Unix.closedir;
    !mx

  (* Inside ./app/var/lib/ (next to target) *)
  let id_next ~bytes_per_item ~items_per_page base =
    let bas = base |> Uri.path in
    let bas = bas |> Filename.concat (Filename.dirname target |> Filename.dirname) in
    let dir = bas |> Filename.dirname
    and fi = bas |> Filename.basename in
    (* get the previously highest index number and filename base *)
    let mxp = (dir,fi) |> max_page_index in
    (* figure out where to put, a.k.a. new index number and filename base *)
    let n,ba =
      if mxp < 0
      then 0,Printf.sprintf "%s/%s-%d" dir fi 0
      else
        let ba = Printf.sprintf "%s/%s-%d" dir fi mxp in
        let n = (try (ba ^ ".idx" |> Unix.stat).st_size
                 with _ -> items_per_page) / (bytes_per_item + 1) in
        if n < items_per_page
        then n,ba
        else 0,Printf.sprintf "%s/%s-%d" dir fi (mxp+1)
    in
    let path = ba ^ "/" in
    let fragment = n |> Int.to_string in
    let u = Uri.make ~path ~fragment () in
    Rfc4287.Id (Uri.to_string u), ba ^ ".idx"

  let to_idx pos fidx =
    File.out_channel false fidx (fun oc ->
        output_bytes oc pos;
        output_char oc '\n' );
    fidx

  open Rfc4287

  let other_feeds (e : Entry.t) =
    let tb = Uri.make ~path:"o/t/" () in
    let open Category in
    let tag (_,(Term (Single p)),_) =
      Uri.make ~path:(p ^ "/") () |> Uri.resolve "https" tb in
    let db = Uri.make ~path:"o/d/" () in
    let day (Rfc3339 iso) =
      let p = String.sub iso 0 10 in
      Uri.make ~path:(p ^ "/") () |> Uri.resolve "https" db in
    (e.published |> day) :: (e.categories |> List.map tag)

  let refresh ~base ~title ~author _num fed =
    let pa = Uri.path fed in
    let (dir,fi) = (Filename.dirname pa, Filename.basename pa) in
    let first = max_page_index (dir,fi) in
    let self = fed in
    let fix = Printf.sprintf "%s/%s-%d%s" dir fi first ".idx" in 
    let es = File.in_channel fix TwoPad10.from_channel in
    Feed.atom_signals
      ~base
      ~self
      ~title
      ~author
      ~first
      es
end

let id_to_b (Rfc4287.Id id) =
  id |> Bytes.of_string

(* add csexp entry to .s and return n id,position triple *)
let add_1_csx oc sx =
  let ol = pos_out oc in
  sx |> Csexp.to_channel oc;
  let ne = pos_out oc in
  let id = match sx |> Rfc4287.Entry.decode with
    | Error _ -> None
    | Ok r    -> Some r.id in
  (id,(ol,ne))

(* if Some id call fkt with id->(ol,ne) *)
let add_1_p fkt = function
  | (None,_v)    -> Logr.warn (fun m -> m "add a strut?")
  | (Some id,v)  -> fkt (id_to_b id, v |> TwoPad10.encode |> Bytes.of_string)

let remake_idx fn idx =
  (* - read all csexps from the source *)
  let ic = open_in_gen [ Open_binary; Open_rdonly ] 0 fn in
  let* sxs = Csexp.input_many ic in
  close_in ic;
  (* copy fn content as csexps to tmp file fn' *)
  let fn' = fn ^ "~" in
  let oc = open_out_gen [ Open_binary; Open_wronly ] File.pFile fn' in
  let cp_csx oc sxs sx = (add_1_csx oc sx) :: sxs in
  let pos = List.fold_left (cp_csx oc) [] sxs in
  close_out oc;
  (* recreate cdb *)
  let none _ = false in
  let add_all fkt = List.iter (add_1_p fkt) pos in
  let _ = Mapcdb.add_many none add_all idx in
  (* swap tmp for real *)
  Unix.rename fn' fn;
  Ok fn

let save
    ?(items_per_page = 50)
    ?(fn = target)
    ?(fn_idx = fn_idx)
    ?(_fn_url = fn_url)
    ?(_fn_t = fn_t)
    e =
  Logr.debug (fun m -> m "Storage.save %s" fn);
  let bytes_per_item = 21 (* index item *) in
  let bas = Uri.make ~path:"o/p/" () in
  let id,fidx = Feed.id_next ~bytes_per_item ~items_per_page bas in
  let e = {(e : Rfc4287.Entry.t) with id} in
  (* append entry to storage .s *)
  let p0 = try (Unix.stat fn).st_size with _ -> 0 in
  let mode = [ Open_append; Open_binary; Open_creat; Open_wronly ] in
  File.out_channel false ~mode fn (fun oc ->
      e
      |> Rfc4287.Entry.encode
      |> Csexp.to_channel oc
    );
  let p1 = (Unix.stat fn).st_size in
  let pos = (p0,p1)
            |> TwoPad10.encode
            |> Bytes.of_string in
  assert (bytes_per_item == (pos |> Bytes.length));
  (* append to the global id->pos index *)
  let _ = Mapcdb.add (id_to_b e.id) pos fn_idx in
  Logr.warn (fun m -> m "@TODO append url->id to urls.cdb");

  (* append to main feed page index *)
  let _ = Feed.to_idx pos fidx in

  (* all other feeds' pages. Inside ./app/var/lib/ *)
  e |> Feed.other_feeds |> List.iter (fun f ->
      let _,fidx = Feed.id_next ~bytes_per_item ~items_per_page f in
      Logr.debug (fun m -> m "feed %s" fidx );
      let _ = Feed.to_idx pos fidx in
      ()
    );
  e

let purge ?(_fn = target) _id =
  (* find start and stop position *)
  (* create a filler buffer of required size *)
  (* overwrite the data *)
  (* find the feeds containing the id and mark them for refresh *)
  (* purge id from indexes? *)
  Error "not implemented yet"

let select ?(_fn = target) _id =
  (* find start and stop positions *)
  Error "not implemented yet"

(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

(*
 * https://www.w3.org/TR/activitystreams-core/
 * https://www.w3.org/TR/activitystreams-core/#media-type
 *)

let cgi' = "seppo.cgi"
let apub = "activitypub/"
let uj u = `String (u |> Uri.to_string)
let ( ^/ ) a b =
  let p = Uri.path a in
  let p' = p ^ b in
  Uri.with_path a p'
let ( >>= ) = Result.bind
let to_result none = Option.to_result ~none
let chain a b =
  let f a = Ok (a, b) in
  Result.bind a f

let write oc j =
  Ezjsonm.to_channel ~minify:false oc j;
  Ok ""

let json_from_file fn =
  let ic = open_in_gen  [ Open_rdonly; Open_binary ] 0 fn in
  let j = Ezjsonm.from_channel ic in
  close_in ic;
  Ok j

module PubKeyPem = struct

  let pub_from_pem s =
    let s = match s with
      | `String s -> s
      | _ -> "" in
    match s
          |> Cstruct.of_string
          |> X509.Public_key.decode_pem with
    | Ok (`RSA _ as k) -> Ok k
    | _ -> Error "public key is fishy"

  let check (`RSA k) =
    Logr.warn (fun m -> m "@TODO PubKeyPem.check." );
    Ok (`RSA k)

  let target = apub ^ "id_rsa.pub.pem"
  let pk_pem = "app/etc/id_rsa.priv.pem"
  let key_pems = (pk_pem, target) (* order matters, pk first, pub second *)

  let pk_rule : Make.t = {
    target = pk_pem;
    srcs =  [];
    fresh = Make.Missing;
    build = fun _pre _ oc _ ->
      Logr.debug (fun m -> m "create private key pem." );
      (* https://discuss.ocaml.org/t/tls-signature-with-opam-tls/9399/3?u=mro
       * $ openssl genrsa -out app/etc/id_rsa.priv.pem 2048
      *)
      let module PSS_SHA512 =
        (* really required? *)
        Mirage_crypto_pk.Rsa.PSS (Mirage_crypto.Hash.SHA512) in
      (* https://github.com/mirage/ocaml-dns/blob/be6c384e9a6fc1de9b8fd3d055371e88a1465a87/app/ocertify.ml#L19 *)
      let key = Mirage_crypto_pk.Rsa.generate ~bits:2048 () in
      (* https://github.com/hannesm/conex/blob/24547cd746915431c72ee7e929d39208f65f4100/mirage-crypto/conex_mirage_crypto.ml#L74 *)
      X509.Private_key.encode_pem (`RSA key)
      |> Cstruct.to_string
      |> output_string oc;
      Ok ""
  }

  let rule : Make.t = {
    target;
    srcs = [ pk_pem ];
    fresh = Make.Outdated;
    build = fun _pre _ oc deps ->
      Logr.debug (fun m -> m "create public key pem." );
      match deps with
      | [ fn_priv ] -> (
          assert (fn_priv = pk_pem);
          match
            fn_priv
            |> File.to_string
            |> Cstruct.of_string
            |> X509.Private_key.decode_pem
          with
          | Ok (`RSA key) ->
                    (*
                    https://github.com/hannesm/conex/blob/24547cd746915431c72ee7e929d39208f65f4100/mirage-crypto/conex_mirage_crypto.ml#L77
                    *)
            let pub = Mirage_crypto_pk.Rsa.pub_of_priv key in
            X509.Public_key.encode_pem (`RSA pub)
            |> Cstruct.to_string
            |> output_string oc;
            Ok ""
          | Ok _
          | Error _ ->
            Error "error, proper message not implemented yet,")
      | l ->
        Error
          (Printf.sprintf
             "rule must have exactly one dependency, not %d"
             (List.length l))
  }

  let rulez = [ pk_rule; rule ]

  let make pre =
    Make.make_1 pre rule

  let pk_from_pem (fn_priv, fn_pub) =
    (* https://mirleft.github.io/ocaml-tls/doc/tls/X509_lwt/#val-private_of_pems *)
    X509_lwt.private_of_pems ~cert:fn_pub ~priv_key:fn_priv

  let sign pk data =
    (*
     * https://discuss.ocaml.org/t/tls-signature-with-opam-tls/9399/9?u=mro
     * https://mirleft.github.io/ocaml-x509/doc/x509/X509/Private_key/#cryptographic-sign-operation
     *)
    X509.Private_key.sign `SHA256 ~scheme:`RSA_PKCS1 pk (`Message (Cstruct.of_string data))
    |> Result.get_ok
    |> Cstruct.to_string
    |> Base64.encode_exn

  let verify _pk _sig str = Ok str
end

module Followers = struct

  let target = apub ^ "followers.json"

  let js_gen num kind (actr : Uri.t) =
    let self = actr ^/ target in
    let sj s = `String s in
    let ij i = `Float (Float.of_int i) in
    Ok
      (`O [
          ("@context", `A [ "https://www.w3.org/ns/activitystreams" |> sj; ] );
          ("id", self |> uj);
          ("type", "OrderedCollection" |> sj);
          ("totalItems", num |> ij);
          ("first", actr ^/ (kind ^ "-4.sjon") |> uj);
        ])

  let jsonm = js_gen 23 "followers"

  let rule : Make.t = {
    target;
    srcs = [ Cfg.Base.fn ];
    fresh = Make.Outdated;
    build = fun _pre _ oc _newer ->
      Cfg.Base.(from_file fn)
      >>= jsonm
      >>= write oc
  }

  let rulez = [ rule ]
end

module Following = struct
  let target = apub ^ "following.json"

  let jsonm = Followers.js_gen 42 "following"

  let rule : Make.t = {
    target;
    srcs = [ Cfg.Base.fn ];
    fresh = Make.Outdated;
    build = fun _pre _ oc _newer ->
      Cfg.Base.(from_file fn)
      >>= jsonm
      >>= write oc
  }

  let rulez = [ rule ]
end

module Liked = struct
  let target = apub ^ "liked.json"

  let jsonm = Followers.js_gen 1001 "liked"

  let rule : Make.t = {
    target;
    srcs = [ Cfg.Base.fn ];
    fresh = Make.Outdated;
    build = fun _pre _ oc _newer ->
      Cfg.Base.(from_file fn)
      >>= jsonm
      >>= write oc
  }

  let rulez = [ rule ]
end

(* A person actor object. https://www.w3.org/TR/activitypub/#actor-objects *)
module Person = struct

  type t = T of Ezjsonm.t

  let write' oc (T j) = write oc j

  let get_txt_opt p (j : t) : string option =
    match j with
    | T (`O _ as j') ->
      (match Ezjsonm.find j' p with
       | `String s -> Some s
       | _ -> None
      )
    | _ -> None

  let get_txt' p j =
    match get_txt_opt p j with
    | None -> assert false
    | Some u -> u

  let get_uri_opt p j =
    Option.bind
      (get_txt_opt p j)
      (fun v -> Some (v |> Uri.of_string))

  let get_uri' p j =
    get_txt' p j |> Uri.of_string

  let get_id = get_uri' [ "id" ]
  let get_type = get_txt' [ "type" ]
  let get_inbox = get_uri' [ "inbox" ]

  let of_jsonm (j' : Ezjsonm.t) : (t, 'a) result =
    let j = T j' in
    let _ = get_id j
    and _ = get_type j
    and _ = get_inbox j in
    Ok j

  let json _pubdate (publicKeyPem, ((pro : Cfg.Profile.t), (uid, base))) : (t, 'a) result =
    let base = Uri.with_userinfo base None in
    let actr = base ^/ apub
    and sj s = `String s
    and name = pro.title
    and summary = pro.bio
    and preferredUsername = uid in
    `O [
      ("@context", `A [
          "https://www.w3.org/ns/activitystreams" |> sj;
          "https://w3id.org/security/v1" |> sj;
(*
          `O [ ("manuallyApprovesFollowers", "as:manuallyApprovesFollowers" |> sj); ];
*)        ] );
      ("id", actr |> uj);
      ("type", "Person" |> sj);
      ("following", base ^/ Following.target |> uj);
      ("followers", base ^/ Followers.target |> uj);
      ("liked", base ^/ Liked.target |> uj);
      ("inbox", base ^/ cgi' ^ "/" ^ apub ^ "inbox.json" |> uj);
      ("outbox", actr ^/ "outbox.json" |> uj);
      ("preferredUsername", preferredUsername |> sj);
      ("name", name |> sj);
      ("summary", summary |> Http.plain2html |> sj);
      (*    ("url", actr |> js); *)
      (*    ("manuallyApprovesFollowers", `Bool false);   *)
      ("publicKey", `O [
          ("id", actr ^/ "#main-key" |> uj);
          ("owner", actr |> uj);
          ("publicKeyPem", publicKeyPem |> sj);
        ] );
      ("attachment", `A [
          `O [
            ("name", "Support" |> sj);
            ("value", "https://Seppo.Social/support" |> Http.plain2html |> sj);
            ("type", "PropertyValue" |> sj);
          ];
          `O [
            ("name", "Generator" |> sj);
            ("value", "https://Seppo.Social" |> Http.plain2html |> sj);
            ("type", "PropertyValue" |> sj);
          ];
          `O [
            ("name", "Funding" |> sj);
            ("value", "https://nlnet.nl/project/Seppo/" |> Http.plain2html |> sj);
            ("type", "PropertyValue" |> sj);
          ];
        ]);
      ("icon", `O [
          ("type", "Image" |> sj);
          ("mediaType", Http.Mime.img_jpeg |> sj);
          ("url", base ^/ "me-avatar.jpg" |> uj);
        ] );
      ("image", `O [
          ("type", "Image" |> sj);
          ("mediaType", Http.Mime.img_jpeg |> sj);
          ("url", base ^/ "me-banner.jpg" |> uj);
        ] );
      (*   ("tag", `A []);
           ("endpoints", `O [
             ("sharedInbox", base ^ cgi' ^ "/" ^ apub ^ "inbox" |> sj);
           ]);
      *)
    ]
    |> of_jsonm

  let target = apub ^ "index.json"

  let rule : Make.t =
    {
      target;
      srcs = [
        Auth.fn;
        Cfg.Profile.target;
        PubKeyPem.target;
        Followers.target;
        Following.target;
        Liked.target;
      ];
      fresh = Make.Outdated;
      build = fun pre _ oc _newer ->
        let now = Ptime_clock.now () in
        Cfg.Base.(make pre >>= from_file)
        >>= chain Auth.(make pre >>= uid_from_file)
        >>= chain Cfg.Profile.(make pre >>= from_file)
        >>= chain (PubKeyPem.make pre >>= File.cat)
        >>= json now
        >>= write' oc
    }

  let make pre = Make.make_1 pre rule

  let rulez =
    PubKeyPem.rulez
    @ Followers.rulez
    @ Following.rulez
    @ Liked.rulez
    @ [ rule ]

  let from_file fn : (t, 'a) result =
    fn
    |> json_from_file
    >>= of_jsonm
end

module Profile = struct
  let http_get u =
    let headers = [ Http.H.acc_act_json ] |> Cohttp.Header.of_list in
    Lwt.bind
      (Http.get_json Result.ok ~headers u)
      Lwt.return
    |> Lwt_main.run

  let get_pubkey_pem j = Ok (Ezjsonm.find j ["publicKey"; "publicKeyPem"])
end

module Activity = struct

  type t = T of string

  let ty s = match s |> String.lowercase_ascii with
    | "like"    -> Ok (T "Like")
    | "dislike" -> Ok (T "Dislike")
    | _         -> Error ("Activity '" ^ s ^ "' not supported.")

  let make_like me (activity : t) _pubdate objec other =
    let sndr = me |> Uri.to_string
    and T acty = activity
    and object' = objec |> Uri.to_string
    and rcpt = other |> Uri.to_string
    and sj s = `String s in
    let make_id sndr object' = Uri.to_string (Uri.with_fragment (Uri.of_string (sndr ^ "likes.json")) (Some object')) in
    (* still more than at https://www.w3.org/TR/activitystreams-core/#activities *)
    `O [
      ("@context", "https://www.w3.org/ns/activitystreams" |> sj);
      ("type", acty |> sj);
      ("id", (make_id sndr object') |> sj);
      ("object", object' |> sj);
      ("actor", sndr |> sj);
      ("to", `A [ rcpt |> sj ]);
      ("cc", `A [ "https://www.w3.org/ns/activitystreams#Public" |> sj ]);
    ]

  let digest s =
    "SHA-256=" ^ (s
                  |> Cstruct.of_string
                  |> Mirage_crypto.Hash.SHA256.digest
                  |> Cstruct.to_string
                  |> Base64.encode_exn)


  (** e.g. https://tube.network.europa.eu/w/aTx3DYwH1km2gTEn9gKpah
   *
   * $ curl -H 'accept: application/activity+json' 'https://tube.network.europa.eu/w/aTx3DYwH1km2gTEn9gKpah'
   * $ curl -H 'accept: application/activity+json' 'https://tube.network.europa.eu/accounts/edps'
  *)
  let like' (act_type : t) post_uri (me : Person.t) =
    let open Cohttp in
    let open Cohttp_lwt in
    (* we need the sender and recipient actor profiles *)
    (* https://github.com/roburio/http-lwt-client/blob/main/src/http_lwt_client.ml *)
    let post_attributed_to json =
      let extract3tries k0 k1 j =
        match Ezjsonm.find j [ k0 ] with
        | `String s -> Some s
        | `A (`String s :: _) -> Some s
        | `A ((`O _ as hd) :: _) -> (
            (* ignore 'type' *)
            match Ezjsonm.find hd [ k1 ] with
            | `String s -> Some s
            | _ -> None)
        | _ -> None
      in
      json
      |> extract3tries "attributedTo" "id"
      |> to_result (* TODO examine the http response code? *) "attribution not found"
      >>= fun v -> Ok (Uri.of_string v)
    in
    let hdrs = [ Http.H.acc_act_json ] in
    let headers = hdrs |> Header.of_list in
    Lwt.bind
      (Http.get_json post_attributed_to ~headers post_uri)
      (function
        | Error _ as e -> Lwt.return e
        | Ok (act_uri : Uri.t) ->
          Lwt.bind
            (Http.get_json Result.ok ~headers act_uri)
            (fun j ->
               match j >>= Person.of_jsonm with
               | Error _ as e -> Lwt.return e
               | Ok pro ->
                 let _ = Person.make "" in
                 Lwt.bind
                   (PubKeyPem.pk_from_pem PubKeyPem.key_pems)
                   (fun (_pub, pk) ->
                      let date = Ptime_clock.now ()
                      and sndr = me |> Person.get_id
                      and rcpt = pro |> Person.get_id
                      and inbx = pro |> Person.get_inbox in
                      let body = make_like sndr act_type date post_uri rcpt |> Ezjsonm.to_string in
                      let headers = Http.signed_headers (PubKeyPem.sign pk) sndr date (digest body) inbx in
                      let headers = Http.H.add' headers Http.H.ct_json in
                      let headers = Header.add_list headers hdrs in
                      Logr.info (fun m -> m "-> http POST %s" (inbx |> Uri.to_string));
                      Lwt.bind
                        (Http.post ~headers (`String body) inbx)
                        (function
                          | Error _ as e -> Lwt.return e
                          | Ok (_resp, body) ->
                            Lwt.bind
                              (body |> Body.to_string)
                              (fun b ->
                                 Logr.debug (fun m -> m "%s" b);
                                 Lwt.return (Ok post_uri))
                        )
                   )
            )
      )

  let like aty uri act =
    aty |> ty
    >>= fun v -> Ok (like' v uri act)

end

(* https://tools.ietf.org/html/rfc7033
*)
module Webfinger = struct
  module Client = struct
    type l = Localpart of string
    type h = Host of string
    type t = Finger of (l * h)

    module P = struct
      let handle = Tyre.conv
          (fun (l, h) -> Finger (Localpart l, Host h))
          (fun (Finger (Localpart l, Host h)) -> l,h)
          (* https://datatracker.ietf.org/doc/html/rfc7565#section-7
           * https://datatracker.ietf.org/doc/html/rfc3986#section-2.3
          *)
          Tyre.(char '@' *> pcre "[a-zA-Z0-9\\-._~!$&'()*+,;=]+" <* char '@' <&> pcre "([a-zA-Z0-9-]+\\.)*[a-zA-Z0-9-]+")

      let handle' = handle |> Tyre.compile
    end

    let from_uri u =
      match (u |> Uri.user), (u |> Uri.host) with
      | Some usr, Some hos ->
        Ok (Finger (Localpart usr, Host hos))
      | _ ->
        Error "uri must have user and host set, e.g. foo@example.com."

    let from_string =
      Tyre.exec P.handle'

    let to_string =
      Tyre.eval P.handle

    let to_uri (Finger (Localpart us, Host ho)) =
      Uri.make ~scheme:"webfinger" ~userinfo:us ~host:ho ()

    let get_profile_uri j =
      let r = (match Ezjsonm.find j ["links"] with
          | `A l -> l
          | _ -> [])
              |> List.find_map
                (function
                  | `O d -> (
                      let hrf = d |> List.assoc_opt "href"
                      and rel = d |> List.assoc_opt "rel"
                      and typ = d |> List.assoc_opt "type" in
                      match rel, typ, hrf with
                      | Some `String "self", Some `String t, Some `String h ->
                        Logr.debug (fun m -> m "has rel = self %s %s" t h);
                        if t |> Http.Mime.is_app_json
                        then Some h
                        else None
                      | _ -> None)
                  | _ -> None)
      in
      match r with
      | None -> Error "rel = 'self' not found"
      | Some x -> Ok (Uri.of_string x)

    let well_known_uri (Finger (Localpart local, Host h)) =
      Uri.make
        ~scheme:"https"
        ~host:h
        ~path:("/" ^ ".well-known/webfinger")
        ~query:[("resource", ["acct:" ^ local ^ "@" ^ h])]
        ()

    let http_get w =
      let ep = w |> well_known_uri in
      let headers = [ Http.H.acc_act_json ] |> Cohttp.Header.of_list
      and ok (r : Cohttp.Response.t) b =
        let _ = Cohttp.Header.fold (fun k v c ->
            (match k with
             | "Content-Type" ->
               if not (v |> Http.Mime.is_app_json)
               then Logr.err (fun m -> m "I should answer with content-type %s\n\
                                          (but did %s),\n\
                                          so as other servers properly understand me. Some are quite fussy about such."
                                 Http.Mime.app_act_json v)
             | _ -> Logr.debug (fun m -> m "  header %s: '%s'" k v));
            c)
            r.headers () in
        Result.ok b in
      Lwt.bind
        (Http.get_json_2 ok ~headers ep)
        (Lwt.return)
      |> Lwt_main.run
  end

  let jsonm (uid, bas) =
    bas |> Uri.host |> to_result "I need a host" >>=
    fun host -> bas |> Uri.scheme |> to_result "I need a scheme" >>=
    fun schem ->
    let path = Uri.path bas in
    let base = schem ^ "://" ^ host ^ path in
    let id = base ^ apub in
    let sj s = `String s in
    Ok
      (`O [
          ("subject", "acct:" ^ uid ^ "@" ^ host |> sj);
          ("aliases", `A [ id  |> sj ]);
          ( "links", `A [
                `O [
                  ("href", id |> sj);
                  ( "rel", "self" |> sj);
                  ("type", Http.Mime.app_act_json |> sj);
                ];
                `O [
                  ("href", base |> sj);
                  ( "rel", "http://webfinger.net/rel/profile-page"  |> sj);
                  ("type", Http.Mime.text_html |> sj);
                ];
                `O [
                  ("href", base |> sj);
                  ( "rel", "alternate" |> sj);
                  ("type", Http.Mime.app_atom_xml |> sj);
                ];
                `O [
                  ( "rel", "http://ostatus.org/schema/1.0/subscribe" |> sj);
                  ("template", base ^ cgi' ^ "/ostatus/authorize?uri={uri}" |> sj);
                ];
              ] );
        ])

  (* @TODO for now the according .htaccess is only written by bin/shell.ml do_create *)
  let target = ".well-known/webfinger" ^ "/index.json"

  let srcs = [ Person.target ]
  let rule : Make.t =
    { target;
      srcs;
      fresh = Make.Outdated;
      build = fun pre _ oc _newer ->
        Cfg.Base.(make pre >>= from_file)
        >>= chain Auth.(make pre >>= uid_from_file)
        >>= jsonm
        >>= write oc;
    }
  let rulez = Person.rulez @ [ rule ]
end


(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( let* ) = Result.bind

let sift_urls e =
  Logr.debug (fun m -> m "%s.%s not implemented yet." "Main" "sift_urls");
  e

let sift_tags e =
  Logr.debug (fun m -> m "%s.%s not implemented yet." "Main" "sift_tags");
  e

let sift_handles e =
  Logr.debug (fun m -> m "%s.%s not implemented yet." "Main" "sift_handles");
  e

let post n =
  Logr.debug (fun m -> m "Main.post");
  let n = Storage.save n in

  Logr.warn (fun m -> m "refresh feed, tagfeeds and outbox");
  (* add the id to all acc. feeds, not just the main stream *)

  (* refresh all feeds accordingly *)
  let* _aut = Auth.fn |> Auth.from_file in
  let* _pro = Cfg.Profile.target |> Cfg.Profile.from_file in
  let _u = Uri.make ~path:"o/p/" () in
  (*  let* _ = Storage.Feed.refresh pro.title aut.uid pro.posts_per_page u in
  *)  (*
   * - write to app/var/lib/new/asezt7b.s
   * - if we have >= 50:
   *   - move the oldest 50 to a new page /var/lib/cur/asezt7b.s
   * - update index cdbs /var/lib/new/url.cdb and ids.cdb
   * - recreate 2 most recent pages and day
  *)
  Ok n

(** *)
let notify n =
  Logr.warn (fun m -> m "notify followers");
  Ok n

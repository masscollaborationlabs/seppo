(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * auth.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

(* Password reset:
 *
 * delete the file app/etc/auth.cfg
*)

let fn = "app/etc/auth.cfg"

let make pre = Make.make pre [] fn

let is_setup = File.exists

let to_file fn (uid, pwd) =
  Logr.debug (fun m -> m "to_file '%s' ..." uid);
  let h = Bcrypt.hash pwd |> Bcrypt.string_of_hash in
  File.out_channel true fn (fun oc ->
      let open Csexp in
      List [ Atom uid; Atom h ] |> to_channel oc;
      Ok fn
    )

let from_file fn =
  File.in_channel fn (fun ic ->
      let open Csexp in
      match input ic with
      | Ok List [ Atom uid; Atom hash ] -> Ok (uid, hash)
      | Error _ as e -> e
      | _ -> Error "invalid credential store"
    )

let uid_from_file fn =
  Logr.debug (fun m -> m "Auth.uid_from_file");
  try
    match from_file fn with
    | Ok (uid, _) -> Ok uid
    | Error _ as e -> e
  with
  | Sys_error e -> Error e

(* https://opam.ocaml.org/packages/safepass/ *)
let chk (uid, pwd) (uid', hash') =
  Logr.debug (fun m -> m "Auth.chk '%s' '%s'" uid "***");
  match hash' |> Bcrypt.hash_of_string |> Bcrypt.verify pwd && uid = uid' with
  | false -> Error "invalid username or password"
  | true ->  Ok uid

let chk_file fn (uid, pwd) =
  match from_file fn with
  | Ok v -> chk (uid, pwd) v
  | Error _ as e -> e

(* https://opam.ocaml.org/packages/safepass/ *)
let verify (uid, pwd) (uid', hash') =
  match uid = uid' with
  | false -> Http.s403
  | true -> (
      match hash' |> Bcrypt.hash_of_string |> Bcrypt.verify pwd with
      | false -> Http.s403
      | true -> Ok uid)

let verify_file fn (uid, pwd) =
  match from_file fn with
  | Ok v -> verify (uid, pwd) v
  | Error _ -> Http.s403


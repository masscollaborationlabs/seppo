(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( >>= ) = Result.bind
let ( let* ) = Result.bind

(** map until the first Error *)
let list f xs =
  let it xs x =
    let* xs = xs in
    let* x = f x in
    Ok (List.cons x xs)
  in
  xs |> List.fold_left it (Ok [])

type single  = Single of string
type multi   = Multi  of string
type rfc3066 = Rfc3066 of string
type rfc3339 = Rfc3339 of string

module Link = struct
  type rel  = Rel of single
  type t = {
    href         : Uri.t;
    rel          : rel option;
  }

  let make ?(rel = None) href = { href; rel }

  let encode r =
    let open Csexp in
    let l = [] in
    let l = (match r.rel with | None -> l
                              | Some (Rel (Single v)) -> Atom "rel" :: Atom v :: l) in
    let l = Atom "href" :: Atom (r.href |> Uri.to_string) :: l in
    List l

  let decode s =
    let open Csexp in
    let rec pairs xs r =
      match xs with
      | Atom "rel"  :: Atom x :: tl -> pairs tl {r with rel=Some (Rel (Single x))}
      | [] -> Ok r
      | _ -> Error "unexpected field"
    in
    match s with
    | List ( Atom "href" :: Atom href :: tl ) ->
      href
      |> Uri.of_string
      |> make
      |> pairs tl
    | _ -> Error "unexpected field"
end

module Category = struct
  type label    = Label of single
  type term     = Term  of single
  type t        = label * term * Uri.t

  let encode (Label (Single l), Term (Single t), u) =
    Csexp.(List [
        Atom "label";  Atom l;
        Atom "term";   Atom t;
        Atom "scheme"; Atom (u |> Uri.to_string)
      ])

  let decode s =
    match s with
    | Csexp.(List [
        Atom "label";  Atom l;
        Atom "term";   Atom t;
        Atom "scheme"; Atom u
      ]) -> Ok (Label (Single l), Term (Single t), u |> Uri.of_string)
    | _ -> Error "expected category but found none"
end

type id      = Id of string

(* Being "super-careful" https://code.mro.name/mro/ProgrammableWebSwartz2013/src/master/content/pages/2-building-for-users.md
 *
 * geohash uses a base 32 https://codeberg.org/mro/geohash/src/commit/ed8e71a03e377b472054a3468979a1cd77fc090d/lib/geohash.ml#L73
 *
 * See also https://opam.ocaml.org/packages/base32/ for int (we need more bits)
*)
module Base24 = struct
  open Optint.Int63

  let alphabet = Bytes.of_string "23456789abcdefghkrstuxyz"
  let base     = 24 |> of_int

  (* encode the n right chars of x *)
  let encode chars x =
    let int_to_char i = i |> Bytes.get alphabet in
    let rec f i x' b =
      match i with
      | -1 -> b
      | _ ->
        rem x' base |> to_int |> int_to_char |> Bytes.set b i;
        f (i - 1) (div x' base) b
    in
    chars |> Bytes.create |> f (chars - 1) x |> Bytes.to_string

  let decode hash =
    let int_of_char c =
      (* if we want it fast, either do binary search or construct a sparse LUT from chars 0-z -> int *)
      match c |> Bytes.index_opt alphabet with
      | None   -> Error c
      | Some i -> Ok i
    and len = hash |> String.length in
    match len <= 7 with
    | false -> Error '_'
    | true  ->
      let rec f idx x =
        match len - idx with
        | 0 -> Ok x
        | _ ->
          let* v = hash.[idx] |> int_of_char in
          v |> of_int
          |> add (mul x base)
          |> f (idx + 1)
      in
      f 0 zero
end

let of_rfc3339 (Rfc3339 t) =
  match t |> Ptime.of_rfc3339 with
  | Error _    -> Error "expected rfc3339"
  | Ok (t,_,_) -> Ok t

module Entry = struct
  type t = {
    id         : id;
    (* assumes an antry has one language for title, tags, content. *)
    lang       : rfc3066;
    author     : Uri.t;
    title      : single;
    published  : rfc3339;
    updated    : rfc3339;
    links      : Link.t list;
    categories : Category.t list;
    content    : multi;
  }

  (** inspired by https://code.mro.name/mro/ShaarliGo/src/cb798ebfae17431732e37a94ee80b29bd3b78911/atom.go#L302 *)
  let id_make t =
    let secs_since_epoch t : Optint.Int63.t =
      let (d',ps') = Ptime.epoch |> Ptime.diff t |> Ptime.Span.to_d_ps in
      let open Optint.Int63 in
      let ( +. ) = add
      and ( *. ) = mul
      and s = Int64.div ps' 1_000_000_000_000L |> of_int64
      and day_s = 24 * 60 * 60 |> of_int
      and d' = d' |> of_int in
      d' *. day_s +. s
    in
    let id = t |> secs_since_epoch |> Base24.encode 7 in
    Logr.debug (fun m -> m "id_make %s" id);
    Id id

  let encode e =
    let Id id         = e.id
    and Rfc3066 lang  = e.lang
    and author        = Uri.make ()
    and Single title     = e.title
    and Rfc3339 published = e.published
    and Rfc3339 updated   = e.updated
    and Multi content = e.content
    in
    Csexp.(
      List [
        Atom "id";         Atom id;
        Atom "lang";       Atom lang;
        Atom "title";      Atom title;
        Atom "author";     Atom (author |> Uri.to_string);
        Atom "published";  Atom published;
        Atom "updated";    Atom updated;
        Atom "links";      List (e.links      |> List.map Link.encode);
        Atom "categories"; List (e.categories |> List.map Category.encode);
        Atom "content";    Atom content;
      ] )

  (* I am unsure if similar to https://opam.ocaml.org/packages/decoders-sexplib/
   * could help.
  *)
  let decode s =
    match s with
    | Csexp.(List [
        Atom "id";         Atom id;
        Atom "lang";       Atom lang;
        Atom "title";      Atom title;
        Atom "author";     Atom author;
        Atom "published";  Atom published;
        Atom "updated";    Atom updated;
        Atom "links";      List links;
        Atom "categories"; List categories;
        Atom "content";    Atom content;
      ]) ->
      let id           = Id id
      and lang         = Rfc3066 lang
      and title        = Single title
      and author       = author |> Uri.of_string
      and published    = Rfc3339 published
      and updated      = Rfc3339 updated
      and content      = Multi content in
      let* links       = links      |> list Link.decode in
      let* categories  = categories |> list Category.decode in
      Ok { id; lang; author; title; published; updated; links; categories; content }
    | _ -> Error "not implemented yet"

  let decode_channel ic =
    let* lst = ic |> Csexp.input_many in
    let* lst = lst |> list decode in
    Ok lst

  let from_text_plain ~published ~lang ~uri title content =
    Logr.debug (fun m -> m "new note %s\n%s" title content);
    let links      = [] in
    let author     = Uri.make ()
    and categories = []
    and content    = Multi content
    and links = (if uri |> Uri.host |> Option.is_none
                 then links
                 else (uri |> Link.make) :: links)
    and title      = Single title
    and updated    = published in
    let* t    = published |> of_rfc3339 in
    let id    = t |> id_make in
    (*
     * - add attributedTo, id
     * - extract microformats (tags, mentions)
     * - via and thanks -> link via
     * - emojis -> tags
     *)
    Ok { id; lang; author; published; updated; links; title; categories; content }

  let from_channel ?(published = Ptime_clock.now ()) ?(lang = Rfc3066 "nl") ic =
    Logr.debug (fun m -> m "rfc4287.from_channel");
    let l1  = input_line ic
    and buf = Buffer.create 512
    and published = Rfc3339 (published |> Ptime.to_rfc3339) in
    let uri = l1 |> Uri.of_string in
    let l1,uri = (if uri |> Uri.host |> Option.is_none
                  then (l1, Uri.empty)
                  else
                    let l1 = try
                        input_line ic
                      with End_of_file -> "" in
                    (l1,uri) ) in
    (try
       while true do
         input_line ic
         |> Buffer.add_string buf;
         Buffer.add_char buf '\n'
       done
     with End_of_file -> ());
    buf
    |> Buffer.contents
    |> from_text_plain ~published ~lang ~uri l1

  let save _ =
  (*
   * - apend to storage csexp (tag feeds)
   * - update indices (id & url cdbs)
   * - recreate recent pages
   * - queue subscriber notification (aka followers)
   *)
    Error "not implemented yet"
end

module Feed = struct
  let
    atom_signals
      ~base
      ~self
      ~title
      ~updated
      ~lang
      ~author
      ~first (* highest index number *)
      _es =
    let numu i u =
      let p = u |> Uri.path in
      let d = p |> Filename.dirname in
      let f = p |> Filename.basename in
      let path = Printf.sprintf "%s/%s-%d" d f i in
      Uri.make ~path ()
    in
    let id = self |> Uri.resolve "https" base |> Uri.to_string in
    let Rfc3339 updated = updated in
    let Rfc3066 lang = lang in
    let first = numu first self in
    let lf i = `Text [ Printf.sprintf "\n%s" (String.make (2*i) ' ') ] in
    let open Markup in
    let r = `End_element :: [] in
    let ns_a = "http://www.w3.org/2005/Atom" in
    let r =
      let link ~rel ~href =
        let href = href |> Uri.resolve "https" base in
        let href = href |> Uri.to_string in
        `Start_element ((ns_a,"link"), [
            (("","href"), href);
            (("","rel"), rel);
          ])
      in
      link ~href:self ~rel:"self"  :: `End_element :: lf 1
      :: link ~href:first ~rel:"first" :: `End_element :: lf 1
      :: link ~href:self  ~rel:"last"  :: `End_element :: lf 0
      (* @TODO previous next *)
      :: r in
    let r =
      `Xml {version = "1.0"; encoding = None; standalone = None} :: lf 0
      :: `PI ("xml-stylesheet","type='text/xsl' href='../../themes/current/posts.xslt'") :: lf 0
      :: `Start_element
        ( (ns_a, "feed"), [
              (("","xmlns"), ns_a);
              (("","xml:lang"), lang);
            ] ) :: lf 1
      :: `Start_element ( (ns_a, "title"), [] ) :: `Text [ title ] :: `End_element :: lf 1
      :: `Start_element ( (ns_a, "id"), [] ) :: `Text [ id ] :: `End_element :: lf 1
      :: `Start_element ( (ns_a, "updated"), [] ) :: `Text [ updated ] :: `End_element :: lf 1
      :: `Start_element ( (ns_a, "generator"), [ (("","uri"), "Seppo.Social"); ] ) :: `Text [ "Seppo - Personal Social Media" ] :: `End_element :: lf 1
      :: `Start_element ( (ns_a, "author"), [] )
      :: `Start_element ( (ns_a, "name"), [] ) :: `Text [ author ] :: `End_element
      :: `End_element :: lf 1
      :: r in
    r
end

    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Media.

Copyright (C) The #Seppo contributors. All rights reserved.


Here are files to enable @myname@example.com lookups (RFC7033). Requests to
/.well-known/webfinger?resource=acct:myname@example.com must return a JSON telling
where to find the profile page etc.

For Apache webservers, the /.well-known/webfinger/.htaccess is a generated symlink
to the same location within the #Seppo! installation. This .htaccess redirects to
index.json next to it.

The .htaccess symlink is regenerated when necessary, but only if it is really a
symlink. Real files are never deleted or modified, as the directory is outside the
#Seppo! installation.

./.htaccess                     linked to by /.well-known/webfinger/.htaccess
./index.json

(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let () =
  let module Cg = Lib.Http.Cgi in
  let module L = Lib.Logr in
  Mirage_crypto_rng_lwt.initialize (module Mirage_crypto_rng.Fortuna);
  (match Cg.request_from_env () |> Cg.consolidate with
   | Ok req ->
     L.open_out "app/var/log/seppo.log";
     let r = Cgi.handle stdout stdin req in
     close_in stdin;
     close_out stdout;
     (* now we might run background tasks *)
     (* L.close_out (); *)
     r
   | Error _ -> Sys.argv
                |> Array.to_list
                |> Shell.exec)
  |> exit

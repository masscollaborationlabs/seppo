(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * cgi.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Lib

let handle oc ic (req : Http.Request.t) =
  let module L = Lib.Logr in
  let ( >>= ) = Result.bind
  and redir_to_config_if_initial (r : Http.Request.t) =
    L.debug (fun m -> m "redir_to_config '%s' '%s' '%s'" req.path_info req.query_string req.request_method);
    match req.path_info, req.request_method, req.query_string with
    | ("", "GET", "") -> Http.s302 (req.script_name ^ Shaarli.Config.path)
    | _ -> Ok r
  and restore perm lst r = Assets.Const.restore_if_nonex perm lst;
    Ok r
  and tnow = Ptime_clock.now ()
  and ban_db = Mapcdb.Cdb Ban.fn in
  (* Onboarding, initial launch:
   *
   * GET "seppo.cgi"
   * - check ban
   * - redirect to seppo.cgi/shaarli/config
   *
   * GET "seppo.cgi/shaarli/config"
   * - check ban
   * - check auth
   * - unpack assets
   * - update assets
   * - form for basic data + uid, pwd.
   *
   * POST "seppo.cgi/shaarli/config"
   * - check ban
   * - check auth
   * - save profile
   * - update assets
   * - redirect to ".."
   *
   *
   * Post:
   *
   * Cases while posting:
   * 1. note (no http url)
   * 2. new url, prefill from GET
   * 3. existing url, prefill from DB
   *
   * ?post=<url>
   *
   * GET seppo.cgi/shaarli/meta
   * - check ban
   * - check auth
   * - GET post url
   * - redirect to seppo.cgi/shaarli?post=
   *   analyze url and destill canonical url, title, description and keywords <html><head><meta>
   *
   * GET seppo.cgi/shaarli?post=
   * - check ban
   * - check auth
   * - unpack assets
   * - update assets
   *   &title=
   *   &description=
   *   &tags= (space separated)
   *
   * POST seppo.cgi/shaarli?post=
   * - check ban
   * - check auth
   * - save post
   * - update assets
   *
  *)
  let dispatch (r : Http.Request.t) =
    let send_file ct p = p
                         |> File.to_string
                         |> Http.clob_send' oc ct
    and send_res ct p = match p |> Res.read with
      | None -> Http.s500
      | Some b -> Http.clob_send' oc ct b
    and ases = Shaarli.ases tnow
    and auth = Shaarli.uid_redir
    and ban  = Ban.escalate ban_db
    and csrf_mk v =
      L.warn (fun m -> m "TODO create and store token");
      Ok ("f19a65cecdfa2971afb827bc9413eb7244e469a8", v)
    and csrf_ck v =
      L.warn (fun m -> m "TODO load and purge token");
      Shaarli.check_token "f19a65cecdfa2971afb827bc9413eb7244e469a8" v
    and form = Http.Form.values in
    match r.path_info, r.request_method with
    | ("/var/lock/challenge" as p,"GET") -> let f = "app" ^ p in f |> send_file Http.Mime.text_plain
    | ("/doap.rdf" as p,          "GET") -> p |> send_res Http.Mime.text_xml
    | ("/LICENSE" as p,           "GET")
    | ("/version" as p,           "GET") -> p |> send_res Http.Mime.text_plain
    | "/search",                  "GET"  -> Http.s501
    | "/shaarli/session",         "GET"  -> r |> ases >>= Shaarli.Session.get oc
    | "/shaarli",                 "GET"  -> r |> ases >>= auth >>= csrf_mk >>= Shaarli.Post.get oc
    | "/shaarli",                 "POST" -> r |> ases >>= auth >>= form ic >>= csrf_ck >>= Shaarli.Post.post tnow oc
    | "/shaarli/login",           "GET"  -> r |> csrf_mk >>= Shaarli.Login.get oc
    | "/shaarli/login",           "POST" -> r |> form ic >>= csrf_ck >>= Shaarli.Login.post tnow ban oc
    | "/shaarli/logout",          "GET"  -> r |> ases >>= Shaarli.Logout.get oc
    | "/shaarli/tools",           "GET"  -> Http.s501
    | "/shaarli/tools",           "POST" -> Http.s501
    | "/shaarli/config",          "GET"  -> r |> ases >>= auth >>= csrf_mk >>= Shaarli.Config.get oc
    | "/shaarli/config",          "POST" -> r |> ases >>= auth >>= form ic >>= csrf_ck >>= Shaarli.Config.post tnow oc
   (* | "/activitypub/monitor.txt", "GET"  -> r |> Activitypub.monitor oc *)
    | "/activitypub/inbox",       "POST" -> Http.s501
    | "/",                        "GET"  -> Http.s302 req.script_name
    | _,                          "GET"  -> r |> Ban.escalate_req ban_db tnow
    | _                                  -> r |> Ban.escalate_req ban_db tnow
  and finish = function
    | Ok () -> 0
    | Error (code, reason, (_ : (string * string) list)) as h ->
      h |> Http.head oc;
      Printf.fprintf oc "%s %s.\n" Http.camel reason;
      if code < 500
      then 0
      else 1 in
  L.info (fun m -> m "%s -> %s %s" req.remote_addr req.request_method (req |> Http.Request.request_uri));
  Ok req
  >>= restore File.pFile Assets.Const.all
  >>= Ban.retry_after_req ban_db tnow
  >>= redir_to_config_if_initial
  >>= dispatch
  |> finish

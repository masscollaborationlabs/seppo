(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Lib
open Lib.Storage

let test_tuple () =
  Logr.info (fun m -> m "storage_test.test_tuple");
  (23,42) |> TwoPad10.encode |> Assert2.equals_string "storage_test.test_tuple 10" "0x00000017-0x0000002a";
  (0x3fff_ffff,42) |> TwoPad10.encode |> Assert2.equals_string "storage_test.test_tuple 20" "0x3fffffff-0x0000002a";
  let (a,b) = "0000000023-0000000042" |> TwoPad10.decode in
  a |> Assert2.equals_int "storage_test.test_tuple 30" 23;
  b |> Assert2.equals_int "storage_test.test_tuple 40" 42;
  assert true

let () =
  Unix.chdir "../../../test/";
  test_tuple();
  assert true

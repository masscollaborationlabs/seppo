(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Media.
 *
 * as2_test.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Lib

let test_webfinger_handle () =
  Logr.debug (fun m -> m "as2_test.testwebfinger_handle");
  let open As2.Webfinger.Client in
  Finger (Localpart "uh", Host "ah.oh")
  |> to_string
  |> Assert2.equals_string "as2_test.test_webfinger_handle 0" "@uh@ah.oh";

  Finger (Localpart "uh", Host "example.com")
  |> well_known_uri
  |> Uri.to_string
  |> Assert2.equals_string "as2_test.test_webfinger_handle 1" "https://example.com/.well-known/webfinger?resource=acct:uh@example.com";

  (match "@uh@ah.oh" |> As2.Webfinger.Client.from_string with
   | Ok (Finger (Localpart "uh", Host "ah.oh")) -> assert true
   | Ok _ -> Assert2.equals_string "as2_test.test_webfinger_handle 1.0" "h" "i"
   | Error _ -> Assert2.equals_string "as2_test.test_webfinger_handle 1.1" "j" "k"
  );

  "@ok@example.com"
  |> As2.Webfinger.Client.from_string
  |> Result.get_ok
  |> As2.Webfinger.Client.to_uri
  |> Uri.to_string
  |> Assert2.equals_string "as2_test.test_webfinger_handle 1" "webfinger://ok@example.com";
  assert true

let test_webfinger () =
  Logr.debug (fun m -> m "as2_test.test_webfinger");
  As2.Webfinger.jsonm ("usr", Uri.of_string "scheme://example.com/p/a/t/h/")
  |> Result.get_ok
  |> Ezjsonm.to_string ~minify:false
  |> Assert2.equals_string "test_webfinger"
    "{
  \"subject\": \"acct:usr@example.com\",
  \"aliases\": [
    \"scheme://example.com/p/a/t/h/activitypub/\"
  ],
  \"links\": [
    {
      \"href\": \"scheme://example.com/p/a/t/h/activitypub/\",
      \"rel\": \"self\",
      \"type\": \"application/activity+json\"
    },
    {
      \"href\": \"scheme://example.com/p/a/t/h/\",
      \"rel\": \"http://webfinger.net/rel/profile-page\",
      \"type\": \"text/html; charset=utf8\"
    },
    {
      \"href\": \"scheme://example.com/p/a/t/h/\",
      \"rel\": \"alternate\",
      \"type\": \"application/atom+xml\"
    },
    {
      \"rel\": \"http://ostatus.org/schema/1.0/subscribe\",
      \"template\": \"scheme://example.com/p/a/t/h/seppo.cgi/ostatus/authorize?uri={uri}\"
    }
  ]
}"

let test_webfinger_sift () =
  Logr.debug (fun m -> m "as2_test.test_webfinger_sift");
  As2.Webfinger.jsonm ("usr", Uri.of_string "https://example.com/sub/")
  |> Result.get_ok
  |> As2.Webfinger.Client.get_profile_uri
  |> Result.get_ok
  |> Uri.to_string
  |> Assert2.equals_string "as2_test.test_webfinger_sift" "https://example.com/sub/activitypub/"

let test_digest_sha256 () =
  Logr.debug (fun m -> m "as2_test.test_digest_sha256");
  let (`Hex h) =
    "" |> Cstruct.of_string |> Mirage_crypto.Hash.SHA256.digest
    |> Hex.of_cstruct
  in
  (* https://de.wikipedia.org/wiki/SHA-2 *)
  h
  |> Assert2.equals_string "digest hex"
    "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
  "" |> Cstruct.of_string |> Mirage_crypto.Hash.SHA256.digest
  |> Cstruct.to_string |> Base64.encode_exn
  (* printf "%s" "" | openssl dgst -sha256 -binary | base64 *)
  |> Assert2.equals_string "digest base64"
    "47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU="

open Lib.As2

let test_digst () =
  Logr.debug (fun m -> m "as2_test.test_digst");
  "" |> Activity.digest
  |> Assert2.equals_string "test_digest"
    "SHA-256=47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=";
  assert true

let test_actor () =
  Logr.debug (fun m -> m "as2_test.test_actor");
  Logr.info (fun m -> m "test_actor");
  "data/2022-07-22-095632-heluecht_--_actor_as.json"
  |> Person.from_file |> Result.get_ok
  |> Person.get_inbox |> Uri.to_string
  |> Assert2.equals_string "test_actor friendica" "https://pirati.ca/inbox/heluecht";
  "data/2022-07-22-103548-mro_--_actor_as_ple.json"
  |> Person.from_file |> Result.get_ok
  |> Person.get_inbox |> Uri.to_string
  |> Assert2.equals_string "test_actor pleroma" "https://pleroma.tilde.zone/users/mro/inbox";
  "data/2022-11-18-173642-mro_--_actor_as_mas.json"
  |> Person.from_file |> Result.get_ok
  |> Person.get_inbox |> Uri.to_string
  |> Assert2.equals_string "test_actor mastodon" "https://digitalcourage.social/users/mro/inbox";
  assert true

let () =
  Unix.chdir "../../../test/";
  test_webfinger_handle ();
  test_webfinger ();
  test_webfinger_sift ();
  test_digest_sha256 ();
  test_digst ();
  test_actor ();
  assert true

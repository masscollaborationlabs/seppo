(entry
  (title "🐫 📺 25 Years of #OCaml: Xavier #@Leroy - Watch OCaml")
  (id ag8gdh2)
  (updated "2022-01-18T08:41:30+01:00")
  (published "2022-01-18T08:37:12+01:00")
  (link "https://watch.ocaml.org/videos/watch/e1ee0fc0-50ef-4a1c-894a-17df181424cb")
  (categories @Leroy AD2021 OCaml PeerTube 🐫 📺)
  (content "\"Professor Xavier Leroy -- the primary original author and leader of the OCaml project -- reflects on 25 years of the OCaml language at his OCaml Workshop #AD2021 keynote speech.\" #PeerTube

via https://discuss.ocaml.org/t/ann-first-announcement-of-bechamel/9164/2?u=mro")
)
(entry
  (title "🖥 Mit der richtigen #Linux -Distribution zum Erfolg | heise online")
  (id ag8gb4e)
  (updated "2022-01-18T08:32:47+01:00")
  (published "2022-01-18T08:13:00+01:00")
  (link "https://heise.de/-6320516")
  (categories Linux 🖥)
  (content "\"… Bei den Linux-Varianten legt auch keine Marketingabteilung fest, dass Ihr Computer plötzlich aufs Abstellgleis gehört. …\" Plan: https://www.heise.de/select/ct/2022/3/2132112064640999627

Mir fehlen einige, über die ich selber gerne mehr wüßte, z.B.

- voidlinux.org
- parabola.nu
- lubuntu.me & xubuntu.org
- slitaz.org")
)

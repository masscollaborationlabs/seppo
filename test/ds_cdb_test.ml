open Lib

let find_1 (fn : string) (k : string) : string option =
  let cdb = fn |> Ds_cdb.open_cdb_in in
  let ret =
    match k |> Bytes.of_string |> Ds_cdb.find_first cdb with
    | Some b -> Some (b |> Bytes.to_string)
    | None -> None
  in
  cdb |> Ds_cdb.close_cdb_in;
  ret

let test_find_1 () =
  "s" |> find_1 "mini.cdb" |> Option.get |> Assert2.equals_string "b" "ß";
  assert (None = ("zzz" |> find_1 "mini.cdb"));
  assert true

let test_make () =
  let fn = "tmp/t.cdb" in
  (try Unix.unlink fn with Unix.Unix_error (_, _, _) -> ());
  let cdb = Ds_cdb.open_out fn in
  Ds_cdb.add cdb ("k" |> Bytes.of_string) ("v" |> Bytes.of_string);
  Ds_cdb.close_cdb_out cdb;

  find_1 fn "k" |> Option.get |> Assert2.equals_string "uu" "v";
  assert true

let () =
  Unix.chdir "../../../test/";
  test_find_1 ();
  test_make ();
  assert true

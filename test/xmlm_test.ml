
open Lib

(* https://erratique.ch/software/xmlm/doc/Xmlm/index.html#outns *)
let test_xml_out () =
  Logr.info (fun m -> m "test_xml_out");
  []
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write empty" "";
  [ `El ((("", "uhu"), []), []) ]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write 1" "<uhu/>";
  [ `El ((("", "e"), [ (("", "a"), "v") ]), [ `Data "foo" ]) ]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write 2" "<e a=\"v\">foo</e>";
  [ `El ((("", "e"), [ (("", "xmlns"), "A") ]), [ `Data "foo" ]) ]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write 3" "<e xmlns=\"A\">foo</e>";
  [ `El ((("A", "e"), [((Xmlm.ns_xmlns, "xmlns"), "A")]), [`Data "foo"]) ]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write 4" "<e xmlns=\"A\">foo</e>";
  [ `El ((("N", "e"), [((Xmlm.ns_xmlns, "n"), "N")]), [`Data "foo"]) ]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write 5" "<n:e xmlns:n=\"N\">foo</n:e>";
  assert true

let () =
  Unix.chdir "../../../test/";
  test_xml_out();
  assert true
